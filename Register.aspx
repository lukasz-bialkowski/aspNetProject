﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Labolatorium" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="container">
	
		<div id="website_logo">
			<a href="index.html">
				<img id="logo_image" src="images/software_icon.png" alt="Logo image"/>
				<span>Soft<b>House</b></span>
			</a>
		</div>
		
		<nav id="menu">
			<div class="option">Main page</div>
			<div class="option">Article</div>
			<div class="option">Download</div>
			<div class="option">Contact</div>
			<div style="clear:both;"></div>
		</nav>
		
		<div id="topbar">
			<div id="topbarR">
				<h1>Co znajdziesz na tej stronie?</h1>
				Lorem esent non hendrerit risus. Nulla id semper sem. Mauris risus mauris, ultrices sed ullamcorper sed, vulputate vel nisi. Aliquam augue ante, mattis in pulvinar vitae, ultrices nec leo. Nulla ultricies augue enim, sit amet semper tellus vulputate sit amet. Maecenas tincidunt, ex eu viverra scelerisque, quam lectus auctor nunc, at pretium nibh lacus in ligula.
			</div>
			<div style="clear:both;"></div>
		</div>
		
		<aside id="sidebar">
			<ul id="sidebar_menu">
				<li><a href="Index.aspx" name="main_page">Main&nbsp;page</a></li>
				<li><a href="Article.aspx">Article</a></li>
				<li><a href="download.html">Download</a></li>
				<li><a href="contactme.html" name="contact_menu" onclick="javascript:set_current(this)" name="contact_menu">Contact</a></li>
				
				<li><a href="personaldataform.php" onclick="javascript:set_current(this)">Personal data form</a></li>
				<li><a href="cookies.php" onclick="javascript:set_current(this)">Cookies</a></li>
					
				<li><a href="Register.aspx" onclick="javascript:set_current(this)">Register</a></li>
				<li><a href="login.php" onclick="javascript:set_current(this)">Login</a></li>
			</ul>
		</aside>

		

			<form id="register" runat="server">
			<h1>Hello !<br>Enter your correct personal data</h1>
			<div class="dottedline"></div>
			  <fieldset> 
			    <legend><b>Your account details</b></legend> 
			    <div> 
			        <label>Imię*
			        <asp:TextBox ID="nametb" runat="server" name="name" type="text"  ></asp:TextBox>&nbsp; 
					&nbsp;</label><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        Display="Dynamic" ErrorMessage="Wprowadz imię. Pole wymagane." 
                        ControlToValidate="nametb" ForeColor="#FF3300"></asp:RequiredFieldValidator>
			    &nbsp;</div>
			    <div> 
			        <label>Nazwisko*
			        <asp:TextBox runat="server" ID="surnametb" name="username" type="text" 
                        placeholder="Enter your user name"  
                        > </asp:TextBox>
					</label>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        Display="Dynamic" ErrorMessage="Wprowadz nazwisko. Pole wymagane." 
                        ForeColor="#FF3300" ControlToValidate="surnametb"></asp:RequiredFieldValidator>&nbsp;</div>
			  	<div> 
			        <label>Hasło*
			        <asp:TextBox runat="server" ID="passwordtb" name="password" type="password"  placeholder="Enter your password" > </asp:TextBox>
					</label>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ErrorMessage="Wprowadz hasło. Pole wymagane." 
                        ControlToValidate="passwordtb" Display="Dynamic" ForeColor="#FF3300"></asp:RequiredFieldValidator>
			    </div>
			    <div> 
			        <label>Powtórz hasło*
			        <asp:TextBox runat="server" ID="password2tb" name="password2" type="password" placeholder="Enter your password" > </asp:TextBox>
					</label>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ErrorMessage="Powtórz hasło." ControlToValidate="password2tb" 
                        Display="Dynamic" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ErrorMessage="Wprowadzone hasła muszą być identyczne." 
                        ControlToCompare="passwordtb" ControlToValidate="password2tb" Display="Dynamic" 
                        ForeColor="#FF3300"></asp:CompareValidator>
			    </div>
			    <div> 
			        <label>Email* 
			        <asp:TextBox runat="server" ID="emailtb" name="email" type="text" placeholder="example@domain.com" ></asp:TextBox>
					</label> 
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ErrorMessage="Wprowadz adres email. Pole wymagane." 
                        ControlToValidate="emailtb" Display="Dynamic" ForeColor="#FF3300"></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ErrorMessage="Wprowadz poprawny adres email." ControlToValidate="emailtb" 
                        Display="Dynamic" ForeColor="#FF3300" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
			    </div>    
			    <div> 
			        <label>Data urodzenia
			        <asp:TextBox runat="server" id="birthtb" name="phone" type="text" placeholder="dd-MM-yyyy" ></asp:TextBox>
					</label> 
			        <asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ControlToValidate="birthtb" ErrorMessage="Wprowadzono przyszłą datę." 
                        ForeColor="#FF3300"></asp:RangeValidator>
			    </div> 
	

			    <button type="submit" value="Submit">Wyślij</button>
 			      <br />
                  <br />
                  <asp:Label ID="userDataLabel" runat="server" Text="Label" Visible="False"></asp:Label>
 			  </fieldset>
			</form>



		</section>	
		
		<footer id="footer">
			<b><i>SoftHouse</i></b> - best software developer. &copy; All rights reserved - 2015.
		</footer>
	
	</body>
</html>
