﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Labolatorium : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RangeValidator1.MaximumValue = DateTime.Today.ToString("dd-MM-yyyy");

        if (IsPostBack)
        {
            Page.Validate();

            if (Page.IsValid)
            {
                string name = nametb.Text;
                string surname = surnametb.Text;
                string email = emailtb.Text;
                string birthDate = birthtb.Text;

                userDataLabel.Text = "Dziękujemy za wypełnienie formularza.</br>Wysłano następujące dane:</br>";
                userDataLabel.Text += String.Format("Imię: {0}{4}Nazwisko: {1}{4}E-mail: {2}{4}Urodzony: {3}{4}", name, surname, email, birthDate, "</br>");
                userDataLabel.Visible = true;
            }
        }

        
    }

    

}